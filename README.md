# Pasos para la ejecución del proyecto localmente en Windows.
## Requerimientos

- 1) Wampserver
- 2) PHP 8.2
- 3) Crear un host virtual en Apache
```
Ruta archivo: C:\wamp64\bin\apache\apache2.4.54.2\conf\httpd.conf
```

```
# VHOST Drupal - 10
<VirtualHost *:80>
  DocumentRoot "${INSTALL_DIR}/www/prueba-desarrollo-drupal/web"
  ServerName prueba-desarrollo-drupal.com
  ServerAlias prueba-desarrollo-drupal.com
  <Directory "${INSTALL_DIR}/www/prueba-desarrollo-drupal/web">
    Options Indexes FollowSymLinks     
    AllowOverride All
    Order Deny,Allow
    Allow from all     
  </Directory>
</VirtualHost>
```

- 4) Crear host en windows
```
Ruta archivo: C:\Windows\System32\drivers\etc\hosts
```

```
127.0.0.1 prueba-desarrollo-drupal.com
```

- 4) Crear e importar base de datos
```
Archivo en la raíz del repositorio
```
- 4) Ejecutar el siguiente comando en la raíz del proyecto:
```
composer install
```

## Si presentan inconvenientes para ver la prueba, pueden contactarme, estare atento.
```
Cell: 3224180840
```
