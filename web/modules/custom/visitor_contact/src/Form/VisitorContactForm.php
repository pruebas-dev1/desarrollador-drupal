<?php

namespace Drupal\visitor_contact\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure visitor_contact settings for this site.
 */
class VisitorContactForm extends ConfigFormBase
{

  /**
   * {@inheritdoc}
   */
  public function getFormId()
  {
    return 'visitor_contact_ing_contact';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames()
  {
    return ['visitor_contact.ing_contact'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state)
  {

    $form = parent::buildForm($form, $form_state);
    $form['#markup'] = $this->t('<div class="alert alert-info" role="alert">Gracias por contactarnos, una vez leído el mensaje nos comunicaremos lo más pronto posible.</div>');
    $form['title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('<b class="fs-5">Asunto&nbsp;<i class="fa-solid fa-asterisk text-danger"></i>:</b>'),
      '#attributes' => [
        'required' => true,
      ],
      // '#default_value' => $this->config('visitor_contact.title')->get('title'),
    ];
    $form['body'] = [
      '#type' => 'textarea',
      '#title' => $this->t('<b class="fs-5">Contenido&nbsp;<i class="fa-solid fa-asterisk text-danger"></i>:</b>'),
      '#attributes' => [
        'required' => true,
      ],
      // '#default_value' => $this->config('visitor_contact.body')->get('body'),
    ];
    $form['email'] = [
      '#type' => 'email',
      '#title' => $this->t('<b class="fs-5">Email&nbsp;<i class="fa-solid fa-asterisk text-danger"></i>:</b>'),
      '#attributes' => [
        'required' => true,
      ],
      // '#default_value' => $this->config('visitor_contact.email')->get('email'),
    ];
    $form['#theme'] = 'visitor_contact_ing_contact_form';
    // var_dump($form);exit();
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state)
  {

    // Validaciones Title
    if (empty($form_state->getValue('title'))) {
      $form_state->setErrorByName('title', $this->t('Este campo es obligatorio'));
    }
    // Validaciones Body
    if (empty($form_state->getValue('body'))) {
      $form_state->setErrorByName('body', $this->t('Este campo es obligatorio'));
    }
    // Validaciones Email
    $sEmail = $form_state->getValue('email');
    if (empty($sEmail)) {
      $form_state->setErrorByName('email', $this->t('Este campo es obligatorio'));
    }
    if (!$this->valid_email($sEmail)) {
      $form_state->setErrorByName('email', $this->t('El correo electrónico <b>(' . $sEmail . ')</b> no es válido, por favor validarlo.'));
    }

    parent::validateForm($form, $form_state);
  }

  private function valid_email($str)
  {
    return (!preg_match("/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,6}$/ix", $str)) ? FALSE : TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state)
  {

    $aField = $form_state->getValues();
    $aFieldDb = [
      'title' => $aField['title'],
      'body' => $aField['body'],
      'email' => $aField['email'],
    ];

    $query = \Drupal::database();
    $query->insert('ing_contact_form')
      ->fields($aFieldDb)
      ->execute();
    \Drupal::messenger()->addStatus('Gracias por contactarnos, nos comunicaremos lo más pronto posible.', 'status');
    parent::submitForm($form, $form_state);
  }
}
